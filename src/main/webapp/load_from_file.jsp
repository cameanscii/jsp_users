<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.FileReader" %>
<%@ page import="java.io.FileNotFoundException" %><%--
  Created by IntelliJ IDEA.
  User: kamyk
  Date: 28.09.2018
  Time: 19:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    List<User> userList = new ArrayList<>();
    try (BufferedReader br = new BufferedReader(new FileReader("data.txt"))) {
        String linia;
        while ((linia = br.readLine()) != null) {
            User u = new User();
            u.loadFromSerializedLine(linia);

            userList.add(u);
        }
    } catch (FileNotFoundException fnfe) {
        out.println("Error!");
    }

    session.setAttribute("user_list", userList);
    response.sendRedirect("user_list.jsp");
%>
</body>
</html>
