<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: kamyk
  Date: 25.09.2018
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
List<User> users;
    if (session.getAttribute("user_list") != null) { // sprawdzamy czy w sesji znajduje się nasza lista
    // wyciągamy listę userów z sesji jeśli tam jest
    users = (List<User>) session.getAttribute("user_list");
        } else {
        // jeśli listy nie ma w sesji tworzymy nową listę
        users = new ArrayList<>();

    }

    String removeUserId=request.getParameter("user_id");
    Long removeId=Long.parseLong(removeUserId);
    Iterator<User> it = users.iterator();
    while(it.hasNext()){
    User user=it.next();
    if(user.getId()==removeId){
    it.remove();
    break;
    }

    }
    session.setAttribute("user_list", users);
    response.sendRedirect("user_list.jsp");
    %>

</body>
</html>
