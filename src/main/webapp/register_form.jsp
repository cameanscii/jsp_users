        <%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.javagda14.exercise.GENDER" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 24.09.2018
  Time: 20:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form</title>
</head>
<body>

<%

    List<User> users;
    if(session.getAttribute("user_list") != null) {
        users = (List<User>) session.getAttribute("user_list");
    }else {
        users = new ArrayList<>();
    }

    String user_id = request.getParameter("user_id");

    User searchedUser = null;
    boolean female = false;
    if (user_id != null) {
        Long modifiedId = Long.parseLong(user_id);
        female = true;
        Iterator<User> it = users.iterator();
        while (it.hasNext()){
            User user = it.next();
            if (user.getId() == modifiedId){
                searchedUser = user;
                break;
            }
        }

        if(searchedUser!=null){
        female=searchedUser.getGender()==GENDER.FEMALE;
        }
    }
%>

<form action="<%= searchedUser==null ? "register_user.jsp" : "modify_user.jsp"%>" method="get">
    <h2>Form:</h2>
    <input type="hidden" hidden value="<%= searchedUser==null? "" : searchedUser.getId()%>" name="modified_id">
    <div>
        <label for="firstName">First name:</label>
        <input id="firstName" name="firstName" type="text" value="<%=searchedUser==null? "" : searchedUser.getFirstName()%>">
    </div>
    <div>
        <label for="lastName">Last name:</label>
        <input id="lastName" name="lastName" type="text" value="<%=searchedUser==null? "" : searchedUser.getLastName()%>">
    </div>
    <div>
        <label for="username">Username:</label>
        <input id="username" name="username" type="text">
    </div>
    <div>
        <label for="birthdate">Birth date:</label>
        <input id="birthdate" name="birthdate" type="datetime-local" value="<%= searchedUser ==null ? "" : searchedUser.getBirthDate().toString()%>">
    </div>
    <div>
        <label for="address">Address:</label>
        <input id="address" name="address" type="text">
    </div>
    <div>
        <label for="height">Height:</label>
        <input id="height" name="height" type="number" value="<%=searchedUser==null? "" : searchedUser.getHeight()%>" min="60" max="230">
    </div>
    <%-- Gender = input type radio --%>
    <div>
        <label for="gender">Gender:</label>
        <input id="gender" name="gender" type="radio" value="MALE" <%=female ? "" : "checked"%> >MALE</input>
        <input name="gender" type="radio" value="FEMALE" <%=female ? "checked" : ""%> >FEMALE</input>
    </div>

    <input type="submit" value="Register">
</form>
</body>
</html>
