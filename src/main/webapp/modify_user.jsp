<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.javagda14.exercise.GENDER" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %><%--
  Created by IntelliJ IDEA.
  User: kamyk
  Date: 25.09.2018
  Time: 20:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modify User</title>
</head>
<body>
<%

    List<User> users;
    if(session.getAttribute("user_list") != null) {
        users = (List<User>) session.getAttribute("user_list");
    }else {
        users = new ArrayList<>();
    }

    String user_id = request.getParameter("modified_id");

    User searchedUser = null;

    if (user_id != null) {
        Long modifiedId = Long.parseLong(user_id);

        Iterator<User> it = users.iterator();
        while (it.hasNext()){
            User user = it.next();
            if (user.getId() == modifiedId){
                searchedUser = user;
                break;
            }
        }



    }


    searchedUser.setFirstName(request.getParameter("firstName"));
    GENDER gender = GENDER.valueOf(request.getParameter("gender"));
    searchedUser.setGender(gender);
    searchedUser.setLastName(request.getParameter("lastName"));
    searchedUser.setAddress(request.getParameter("address"));
    searchedUser.setHeight(Integer.parseInt(request.getParameter("height")));
    searchedUser.setUsername(request.getParameter("username"));
    searchedUser.setJoinDate(LocalDateTime.now());

    //2018-01-01T01:00
    LocalDateTime birthDate = LocalDateTime.parse(
            request.getParameter("birthdate"),
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));

    searchedUser.setBirthDate(birthDate);

    users.add(searchedUser);
    session.setAttribute("user_list", users);
    response.sendRedirect("user_list.jsp");

%>
</body>
</html>
