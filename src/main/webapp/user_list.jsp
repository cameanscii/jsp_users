<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/25/18
  Time: 6:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User list</title>
</head>
<body>

<%!
    public String generateDeleteLink(long id){
        StringBuilder builder = new StringBuilder();
        builder.append("<a href=\"");
        builder.append("remove_user.jsp?user_id=" + id);
        builder.append("\">remove</a>");

        return builder.toString();
    }
    public String generateModifyLink(long id){
        StringBuilder builder = new StringBuilder();
        builder.append("<a href=\"");
        builder.append("register_form.jsp?user_id=" + id);
        builder.append("\">modify</a>");

        return builder.toString();
    }
%>
<%
    List<User> userList;
    if (session.getAttribute("user_list") != null) {
        userList = (List<User>) session.getAttribute("user_list");
    } else {
        userList = new ArrayList<>();
    }
%>
<table width="100%" border="1" align="center">
<thead>
<th>Id</th>
<th>Imie</th>
<th>Nazwisko</th>
<th>Data Urodzenia</th>
<th></th>
<th></th>

</thead>


<%
    for (User u : userList) {
        out.print("<tr>");
        out.print("<td>");
        out.print(u.getId());
        out.print("</td>");
        out.print("<td>");
        out.print(u.getFirstName());
        out.print("</td>");
        out.print("<td>");
        out.print(u.getLastName());
        out.print("</td>");
        out.print("<td>");
        out.print(u.getBirthDate());
        out.print("</td>");
        out.print("<td>");
        out.print(generateDeleteLink(u.getId()));
        out.print("</td>");
        out.print("<td>");
        out.print(generateModifyLink(u.getId()));
        out.print("</td>");
        out.print("</tr>");


    }
%>
    </table>

<div>

    <form action="save_to_file.jsp">
    <input type="submit" value="save">
    </form>
    <form action="load_from_file.jsp">
        <input type="submit" value="load">
    </form>

</div>

</body>
</html>
