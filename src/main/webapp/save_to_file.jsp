<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.FileWriter" %>
<%@ page import="java.io.FileNotFoundException" %><%--
  Created by IntelliJ IDEA.
  User: kamyk
  Date: 28.09.2018
  Time: 19:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
List<User> userList;
    if (session.getAttribute("user_list") != null) {
    userList = (List<User>) session.getAttribute("user_list");
        } else {
        userList = new ArrayList<>();
        }

        try(PrintWriter writer=new PrintWriter(new FileWriter("data.txt"))){
        for(User u: userList){
            writer.println(u.toSerializedLine());
        }

        }
        catch(FileNotFoundException fnfe){
        out.print("Error!");
        }
        response.sendRedirect("userlist.jsp");
        %>
</body>
</html>
